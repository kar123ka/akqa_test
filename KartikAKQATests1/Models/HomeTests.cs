﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using KartikAKQA.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KartikAKQA.Models.Tests
{
    [TestClass()]
    public class HomeTests
    {
        Home obj = new Home();
        [TestMethod()]
        public void IsValidTest1()
        {
            obj.Number = "1253.23";
            Assert.IsTrue(obj.IsValid());
        }
        [TestMethod()]
        public void IsValidTest2()
        {
            obj.Number = "1253000";
            Assert.IsTrue(obj.IsValid());
        }
        [TestMethod()]
        public void IsValidTest3()
        {
            obj.Number = "002101253.000";
            Assert.IsTrue(obj.IsValid());
        }
        [TestMethod()]
        public void IsValidTest4()
        {
            obj.Number = "1253.23f";
            Assert.IsFalse(obj.IsValid());
        }
        [TestMethod()]
        public void IsValidTest5()
        {
            obj.Number = "1253a.23";
            Assert.IsFalse(obj.IsValid());
        }
        [TestMethod()]
        public void IsValidTest6()
        {
            obj.Number = "1253l";
            Assert.IsFalse(obj.IsValid());
        }
    }
}