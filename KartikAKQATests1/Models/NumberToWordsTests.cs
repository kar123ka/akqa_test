﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using KartikAKQA.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KartikAKQA.Models.Tests
{
    [TestClass()]
    public class NumberToWordsTests
    {
        NumberToWords numberToWords = new NumberToWords();        
        [TestMethod()]
        public void ConvertToWordsTest1()
        {
            string number = "10230.25";
            string words = "Ten Thousand Two Hundred Thirty Dollar And Twenty Five Cents";
            string response = numberToWords.ConvertToWords(number);
            Assert.AreEqual(words, response);            
        }
        [TestMethod()]
        public void ConvertToWordsTest2()
        {
            string number = "010230.25";
            string words = "Ten Thousand Two Hundred Thirty Dollar And Twenty Five Cents";
            string response = numberToWords.ConvertToWords(number);
            Assert.AreEqual(words, response);           
        }
        [TestMethod()]
        public void ConvertToWordsTest3()
        {
            string number = "00200.05";
            string words = "Two Hundred Dollar And Five Cents";
            string response = numberToWords.ConvertToWords(number);
            Assert.AreEqual(words, response);
        }
        [TestMethod()]
        public void ConvertToWordsTest4()
        {
            string number = "500.5";
            string words = "Five Hundred Dollar And Fifty Cents";
            string response = numberToWords.ConvertToWords(number);
            Assert.AreEqual(words, response);
        }
        [TestMethod()]
        public void ConvertToWordsTest5()
        {
            string number = "00.00";
            string words = "Zero Dollar";
            string response = numberToWords.ConvertToWords(number);
            Assert.AreEqual(words, response);
        }
        [TestMethod()]
        public void WholeNumberTest1()
        {
            string number = "527";
            string words = "Five Hundred Twenty Seven";
            string response = numberToWords.WholeNumber(number);
            Assert.AreEqual(words, response);
        }
        [TestMethod()]
        public void WholeNumberTest2()
        {
            string number = "0000527";
            string words = "Five Hundred Twenty Seven";
            string response = numberToWords.WholeNumber(number);
            Assert.AreEqual(words, response);
        }
        [TestMethod()]
        public void WholeNumberTest3()
        {
            string number = "2002552000000";
            string words = "Two Thousand Two Billion Five Hundred Fifty Two Million";
            string response = numberToWords.WholeNumber(number);
            Assert.AreEqual(words, response);
        }
    }
}