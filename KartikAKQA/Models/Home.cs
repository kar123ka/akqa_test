﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace KartikAKQA.Models
{
    public class Home
    {
        public string FullName { get; set; }
        public string Number { get; set; }
        public bool IsValid()
        {
            if (!string.IsNullOrEmpty(Number))
            {
                if (Regex.IsMatch(Number, @"^[0-9]+(\.[0-9]+)?$"))
                {
                    return true;
                }
            }
            return false;
        }
        /// <summary>
        /// Convert number from Number variable to Words and assigne it Number variable
        /// </summary>
        public void NumbertoWords()
        {
            if (!string.IsNullOrEmpty(Number))
            {
                NumberToWords obj = new NumberToWords();
                Number = obj.ConvertToWords(Number);
            }
        }
    }
}