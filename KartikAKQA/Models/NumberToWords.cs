﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KartikAKQA.Models
{
    public class NumberToWords
    {
        /// <summary>
        /// Convert numbers with two digit decimal numbers into words
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        public string ConvertToWords(string number)
        {
            String val = "", wholeNo = number, points = "", andStr = "", pointStr = "";
            String endStr = "";
            try
            {
                int decimalPlace = number.IndexOf(".");
                if (decimalPlace > 0)
                {
                    wholeNo = number.Substring(0, decimalPlace);
                    points = number.Substring(decimalPlace + 1);
                    if (points.Length >2)
                    {
                        points = points.Substring(0, 2);
                    }
                    else if(points.Length ==1)
                    {
                        points = points + "0";
                    }
                    if (Convert.ToInt32(points) > 0)
                    {
                        andStr = "And ";// just to separate whole numbers from points/cents  
                        endStr = "Cents";//Cents  
                        pointStr = WholeNumber(points);
                    }
                }
                wholeNo = WholeNumber(wholeNo).Trim();
                if(string.IsNullOrEmpty(wholeNo)) //if whole number is null
                {
                    wholeNo = "Zero";
                }
                wholeNo = wholeNo + " Dollar";
                val = String.Format("{0} {1}{2} {3}", wholeNo, andStr, pointStr, endStr);
            }
            catch { }
            return val.Trim();
        }
       /// <summary>
       /// Convert Whole Numbers to words
       /// </summary>
       /// <param name="number"></param>
       /// <returns></returns>
        public string WholeNumber(string number)
        {
            string word = "";
            try
            {
                bool isDone = false;//test if already translated  
                //test for zero or digit zero in a nuemric  
                while (number.StartsWith("0"))
                {

                    if (number.Length > 1)
                    {
                        number = number.Substring(1);
                    }
                    else
                    {
                        isDone = true;
                        break;
                    }
                    
                }                
                long numValue = (Convert.ToInt64(number));
                //if ((dblAmt > 0) && number.StartsWith("0"))  
                if (numValue > 0)
                {
                    

                    int numDigits = number.Length;
                    int pos = 0;//store digit grouping  
                    String place = "";//digit grouping name:hundres,thousand,etc...  
                    switch (numDigits)
                    {
                        case 0:
                            isDone = true;
                            break;
                        case 1://ones' range
                            word = Ones(number);
                            isDone = true;
                            break;
                        case 2://tens' range  
                            word = Tens(number);
                            isDone = true;
                            break;
                        case 3://hundreds' range  
                            pos = (numDigits % 3) + 1;
                            place = " Hundred ";
                            break;
                        case 4://thousands' range  
                        case 5:
                        case 6:
                            pos = (numDigits % 4) + 1;
                            place = " Thousand ";
                            break;
                        case 7://millions' range  
                        case 8:
                        case 9:
                            pos = (numDigits % 7) + 1;
                            place = " Million ";
                            break;                      
                        default://Billions's range or above
                            pos = (numDigits % 10) + 1;
                            place = " Billion ";
                            break;
                    }
                    if (!isDone)
                    {//if transalation is not done, continue...(Recursion comes in now!!)                         
                        try
                        {
                            word = WholeNumber(number.Substring(0, pos)) + place + WholeNumber(number.Substring(pos));
                        }
                        catch { }                         
                    }
                    //ignore digit grouping names  
                    if (word.Trim().Equals(place.Trim())) word = "";
                }
            }
            catch { }
            return word.Trim();
        }
        /// <summary>
        /// Convert single number to words
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        private string Ones(string number)
        {
            int num = Convert.ToInt32(number);
            String name = "";
            switch (num)
            {

                case 1:
                    name = "One";
                    break;
                case 2:
                    name = "Two";
                    break;
                case 3:
                    name = "Three";
                    break;
                case 4:
                    name = "Four";
                    break;
                case 5:
                    name = "Five";
                    break;
                case 6:
                    name = "Six";
                    break;
                case 7:
                    name = "Seven";
                    break;
                case 8:
                    name = "Eight";
                    break;
                case 9:
                    name = "Nine";
                    break;
            }
            return name;
        }
        /// <summary>
        /// Convert two digit numbers to Words
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        private string Tens(string number)
        {
            int num = Convert.ToInt32(number);
            String name = null;
            switch (num)
            {
                case 10:
                    name = "Ten";
                    break;
                case 11:
                    name = "Eleven";
                    break;
                case 12:
                    name = "Twelve";
                    break;
                case 13:
                    name = "Thirteen";
                    break;
                case 14:
                    name = "Fourteen";
                    break;
                case 15:
                    name = "Fifteen";
                    break;
                case 16:
                    name = "Sixteen";
                    break;
                case 17:
                    name = "Seventeen";
                    break;
                case 18:
                    name = "Eighteen";
                    break;
                case 19:
                    name = "Nineteen";
                    break;
                case 20:
                    name = "Twenty";
                    break;
                case 30:
                    name = "Thirty";
                    break;
                case 40:
                    name = "Fourty";
                    break;
                case 50:
                    name = "Fifty";
                    break;
                case 60:
                    name = "Sixty";
                    break;
                case 70:
                    name = "Seventy";
                    break;
                case 80:
                    name = "Eighty";
                    break;
                case 90:
                    name = "Ninety";
                    break;
                default:
                    if (num > 0)
                    {
                        name = Tens(number.Substring(0, 1) + "0") + " " + Ones(number.Substring(1));
                    }
                    break;
            }
            return name;
        }
    }
}