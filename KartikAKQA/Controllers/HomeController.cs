﻿using KartikAKQA.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
//using System.Web.Mvc;

namespace KartikAKQA.Controllers
{
    public class HomeController : ApiController
    {
        // GET: Home
        
        [Route("getData")]
        [HttpPost]
        public HttpResponseMessage GetData(Home obj)
        {
            
            if (!string.IsNullOrEmpty(obj.FullName))
            {
                if(obj.IsValid())
                {
                   obj.NumbertoWords();
                   return Request.CreateResponse(HttpStatusCode.OK, obj, Configuration.Formatters.JsonFormatter);
                    //return httpResponse;
                }
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Invalid Number");               
            }
            return Request.CreateResponse(HttpStatusCode.BadRequest, "Please Enter Full Name");                    
        }        
    }
}